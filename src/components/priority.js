import React from 'react';

export class Priority extends React.Component {
  render() {
    var building = this.props.building;
    return (
      <div className='priority'>
        <button type="button" id="increasePriority" onClick={() => this.props.increasePriority(building)}>^</button>
        <button type="button" id="decreasePriority" onClick={() => this.props.decreasePriority(building)}>v</button>
      </div>
    );
  }
}
