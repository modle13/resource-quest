import React from 'react';

export const VillageContext = React.createContext(
    {
        'buildings': [],
        'villagers': [],
        'stores': {},
    },
);
