import React from 'react';

export class VillagerStatus extends React.Component {

  render(props) {
    const villagerStatus = this.props.villagerStatus;
    return <div className="infos">
      { Array.from(Object.keys(villagerStatus)).map(status =>
          <div key={status}>{status} : {villagerStatus[status]}</div> )
      }
    </div>
  }
}
